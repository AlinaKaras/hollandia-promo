import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/account/Login.vue";
import Recover from "../views/account/Recover.vue";
import Signup from "../views/account/Signup.vue";
import Verification from "../views/Verification.vue";
import Production from "../views/Production.vue";

Vue.use(VueRouter);

const titlePrefix = "У каждого своя HOLLANDIA";
const store = require("../store");

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    // redirect: '/coming-soon',
    meta: {
      title: titlePrefix,
    },
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../views/account/Profile.vue"),
    meta: {
      title: titlePrefix,
    },
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      title: titlePrefix + " Авторизация",
    },
  },
  {
    path: "/recover",
    name: "Recover",
    component: Recover,
    meta: {
      title: titlePrefix + " Восстановить пароль",
    },
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup,
    meta: {
      title: titlePrefix + " Регистрация",
    },
  },
  {
    path: "/",
    name: "Verification",
    component: Verification,
    meta: {
      title: titlePrefix + " Вам есть 18 лет?",
    },
  },
  {
    path: "/production",
    name: "Production",
    component: Production,
    meta: {
      title: titlePrefix + " Продукция",
    },
  },
  // { path: "/:pathMatch(.*)*", name: "NotFound", component: Home },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        behavior: "smooth",
        offset: {
          y: 0,
        },
      };
    }
    return { x: 0, y: 0 }; // Go to the top of the page if no hash
  },
});
// function hasQueryParams(route) {
//   return !!Object.keys(route.query).length
// }

// router.beforeEach((to, from, next) => {
//    if(!hasQueryParams(to) && hasQueryParams(from)){
//     next({name: to.name, query: from.query});
//   } else {
//     next()
//   }
// })

export default router;
